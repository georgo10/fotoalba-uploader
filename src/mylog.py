#! /bin/env python
# -*- coding: utf-8 -*-
#
# $Id: mylog.py 20 2008-01-19 11:02:13Z georgo10 $

import time

class mylog:
	debug = False
	
def log(text, level = 0):
	if mylog.debug or level == 1:
		print "[%s] %s" % (time.strftime('%Y/%m/%d %H:%M:%S'), text)
