# Project:          Fotoalba-Uploader for Linux

# Executables
PYUIC 	= pyuic4
PYRCC	= pyrcc4
RM	= rm -rv
MKDIR	= mkdir -p
COPY	= cp

# Directories
BUILDTO = bin

default:	fotoalba
		@echo --- Done.

all:		fotoalba
		@echo --- Done.

clean:
		@echo --- Cleaning...
		$(RM) $(BUILDTO)/
		@echo --- Done.

fotoalba:	fotoalba_main icon
		$(MKDIR) $(BUILDTO)
		@echo --- Copying sources
		$(COPY) src/*.py $(BUILDTO)/
		$(COPY) src/*.sh $(BUILDTO)/
		$(COPY) COPYING $(BUILDTO)/
		$(COPY) CHANGELOG $(BUILDTO)/
		$(COPY) README $(BUILDTO)/

icon:
		$(COPY) src/ui/icon.png $(BUILDTO)/

images_rc:
		$(MKDIR) $(BUILDTO)
		$(PYRCC) src/ui/images.qrc -o $(BUILDTO)/images_rc.py

fotoalba_main:	images_rc
		$(MKDIR) $(BUILDTO)
		@echo --- Creating UI
		$(PYUIC) src/ui/fotoalba_main.ui -o $(BUILDTO)/fotoalba_main.py